# Vue To Do App

## Setup/Usage
- Create a DB (Either SQLite or MySQL and add to config in .env)
- Utilise Valet or `php artisan serve` to host application
- Visit appropriate URL (E.g. vuetodo.test)
- Register an account
- Create a category & tasks as required.

## Features
- Ability to create a task, assign a Category, Description & Due Date
- Ability to Edit, Delete (And restore as required) tasks
- Ability to view tasks in list or Categorised format

## Technology Used
- PHP & Laravel Framework - (https://laravel.com/),
- Vue.js (Front End Framework - https://vuejs.org/),
- Bulma Css (Styling - https://bulma.io/)
- SweetAlert2 (Notifications - https://sweetalert2.github.io/)
- MySQL for DB
- PHPUnit for Testing

## Tests
To run tests, navigate to Application Directory & run:
`php Artisan Test`

## Longer Term Developments
- Audit FrontEnd components to potentially modularise further
- Implementation of FrontEnd Vue Tests as required 
- Implementation of Draggable (https://sortablejs.github.io/Vue.Draggable/)
  to enable resorting of data and to implement a more trello like user experience
- Implementation of Vue Hot Keys (E.g. Tab + N to create a new Task)
- Front End rebuild to use Electron to create a desktop application with state persistence
- Hosted solution implementing Laravel Spark to scaffold SaaS

Feel free to submit a feature request to aidanl_(at)hotmail.co.uk
