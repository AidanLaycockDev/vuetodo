<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTaskRequest;
use App\Http\Requests\DeleteTaskRequest;
use App\Http\Requests\EditTaskRequest;
use App\Http\Requests\RestoreTaskRequest;
use App\Task;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{

    public function Index()
    {
        return view('index');
    }

    public function Get()
    {
        return Task::with('Category')
            ->where('User_id', Auth::id())
            ->get();
    }

    public function Create(CreateTaskRequest $request)
    {
        $data = $request->validated();
        // Append User to Request Data:
        $data['User_id'] = Auth::id();

        $id = Task::create($data)->id;

        return response($id, 200);
    }

    public function Edit(EditTaskRequest $request)
    {
        Task::find($request->id)->update($request->validated());

        return response('Successfully Updated Task', 200);
    }

    public function Delete(DeleteTaskRequest $request)
    {
        Task::destroy($request->id);

        return response('Successfully Deleted Task', 200);
    }

    public function Restore(RestoreTaskRequest $request)
    {
        Task::withTrashed()->find($request->id)->restore();

        return response('Successfully Restored Task', 200);
    }

    public function GetDeleted()
    {
        return Task::with('Category')
            ->where('User_id', Auth::id())
            ->onlyTrashed()
            ->get();
    }

}
