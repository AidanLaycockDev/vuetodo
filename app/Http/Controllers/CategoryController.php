<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\DeleteCategoryRequest;
use App\Http\Requests\EditCategoryRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    public function Get()
    {
        // Return all Categories
        return Category::where('user_id', Auth::id())->get();
    }

    public function Create(CreateCategoryRequest $request)
    {
        $data = $request->validated();
        // Append User to Request Data:
        $data['User_id'] = Auth::id();

        $id = Category::create($data)->id;

        return response($id, 200);
    }

    public function Edit(EditCategoryRequest $request)
    {
        Category::find($request->id)->update($request->validated());

        return response('Category Updated Successfully', 200);
    }

    public function Delete(DeleteCategoryRequest $request)
    {
        Category::destroy($request->id);

        return response('Category Deleted Successfully', 200);
    }
}
