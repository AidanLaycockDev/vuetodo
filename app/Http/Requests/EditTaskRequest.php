<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'bail|required|integer',
            'Title' => 'bail|required|string',
            'Description' => 'string|nullable',
            'Category_id' => 'bail|required|integer',
            'Due_date' => 'date|nullable',
            'Complete' => 'boolean|nullable'
        ];
    }
}
