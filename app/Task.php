<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;

    protected $table = 'tasks';

    protected $fillable = ['Title', 'Description', 'Due_date', 'Complete', 'Category_id', 'User_id'];

    public function Category()
    {
        return $this->hasOne(Category::class, 'id', 'Category_id');
    }

}
