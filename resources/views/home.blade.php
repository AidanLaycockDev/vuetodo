@extends('layouts.app')

@section('content')
<div class="container">
    <div class="columns is-centered">
        <div class="column is-8">
            <div class="card">
                <div class="card-header">
                    <p class="card-header-title">Dashboard</p>
                </div>

                <div class="card-content">
                    <div class="content">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        Welcome! You are now logged in!
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
