@extends('layouts.app')

@section('content')
    <div class="container">
            <v-task-list

                tasks_get="{{ route('GetTasks') }}"
                tasks_create="{{ route('CreateTask') }}"
                tasks_edit="{{ route('EditTask') }}"
                tasks_delete="{{ route('DeleteTask') }}"
                tasks_get_deleted="{{ route('GetDeletedTasks') }}"
                tasks_restore="{{ route('RestoreTask') }}"

                categories_get="{{ route('GetCategories') }}"
                categories_create="{{ route('CreateCategory') }}"
                categories_edit="{{ route('EditCategory') }}"
                categories_delete="{{ route('DeleteCategory') }}"

            ></v-task-list>
    </div>
@endsection
