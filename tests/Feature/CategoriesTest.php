<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoriesTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    protected $user_one;
    protected $user_two;

    public function setUp(): void
    {
        parent::setUp();

        $this->withoutExceptionHandling();

        // Init 2 users for testing purposes:
        $this->user_one = factory(User::class)->create();
        $this->user_two = factory(User::class)->create();
    }

    public function GenerateCategory()
    {
        // Generate a Category:
        return [
            'Title' => $this->faker->sentence
        ];
    }

    /** @test */
    public function a_user_can_see_their_categories()
    {
        $Category = $this->GenerateCategory();

        $this->actingAs($this->user_one)
            ->post('ajax/category/create', $Category)
            ->assertStatus(200);

        $this->actingAs($this->user_one)
            ->get('ajax/category/get')
            ->assertSee($Category['Title']);
    }

    /** @test */
    public function a_user_can_create_a_category()
    {
        $this->actingAs($this->user_one)
             ->post('ajax/category/create', $this->GenerateCategory())
             ->assertStatus(200);
    }

    /** @test */
    public function a_user_can_edit_a_category()
    {
        $id = $this->actingAs($this->user_one)
                   ->post('ajax/category/create', $this->GenerateCategory())
                   ->assertStatus(200);

        $category = $this->GenerateCategory();
        $category['id'] = $id->content();

        $this->actingAs($this->user_one)
            ->post('ajax/category/edit', $category)
            ->assertStatus(200);

        $this->assertDatabaseHas('categories', $category);
    }

    /** @test */
    public function a_user_can_delete_a_category()
    {
        $category = $this->GenerateCategory();

        $id = $this->actingAs($this->user_one)
            ->post('ajax/category/create', $category)
            ->assertStatus(200);

        $this->actingAs($this->user_one)
            ->post('ajax/category/delete', ['id' => $id->content()])
            ->assertStatus(200);

        $this->assertDatabaseMissing('categories', $category);
    }

}
