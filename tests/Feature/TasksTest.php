<?php

namespace Tests\Feature;

use App\Category;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TasksTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    protected $categoryID;

    protected $user_one;
    protected $user_two;

    public function setUp(): void
    {
        parent::setUp();

        //$this->withoutExceptionHandling();

        // Create a Category:
        $this->categoryID = factory(Category::class)->create()->id;

        // Init 2 users for testing purposes:
        $this->user_one = factory(User::class)->create();
        $this->user_two = factory(User::class)->create();
    }

    public function GenerateTask()
    {
        // Generate a Task:
        return [
            'Title' => $this->faker->sentence,
            'Description' => $this->faker->paragraph,
            'Category_id' => $this->categoryID,
            'Due_date' => Carbon::tomorrow(),
            'Complete' => false
        ];
    }

    /** @test */
    public function a_user_can_view_their_tasks()
    {
        // Set User:
        $this->actingAs($this->user_one);

        $task = $this->GenerateTask();

        $this->post('ajax/tasks/create', $task)->assertStatus(200);

        $tasks = $this->get('ajax/tasks/get')->assertSee($task['Title']);
    }

    /** @test */
    public function a_user_can_create_a_task()
    {
        // Set User:
        $this->actingAs($this->user_one);

        $task = $this->GenerateTask();

        $this->post('ajax/tasks/create', $task)->assertStatus(200);

        $this->assertDatabaseHas('tasks', $task);
    }

    /** @test */
    public function a_user_can_edit_a_task()
    {
        // Set User:
        $this->actingAs($this->user_one);

        $task = $this->GenerateTask();

        $id =  $this->post('ajax/tasks/create', $task)->assertStatus(200);

        $this->assertDatabaseHas('tasks', $task);

        // Get Task ID & Update params:
        $task['id'] = $id->content();
        $task['Title'] = $this->faker->sentence;
        $task['Description'] = $this->faker->paragraph;

        $this->post('ajax/tasks/edit', $task)->assertStatus(200);

        $this->assertDatabaseHas('tasks', $task);
    }

    /** @test */
    public function a_user_can_delete_a_task()
    {
        // Set User:
        $this->actingAs($this->user_one);

        $task = $this->GenerateTask();

        $id =  $this->post('ajax/tasks/create', $task)->assertStatus(200);

        $this->post('ajax/tasks/delete', ['id' => $id->content()])->assertStatus(200);

        $task['id'] = $id->content();

        $this->assertSoftDeleted('tasks', $task);
    }

    /** @test */
    public function a_user_can_un_delete_a_task()
    {
        // Set User:
        $this->actingAs($this->user_one);

        $task = $this->GenerateTask();

        $id =  $this->post('ajax/tasks/create', $task)->assertStatus(200);

        $this->post('ajax/tasks/delete', ['id' => $id->content()])->assertStatus(200);

        $task['id'] = $id->content();

        $this->assertSoftDeleted('tasks', $task);

        $tasks = $this->get('ajax/tasks/get')->assertDontSee($task['Title']);

        $this->post('ajax/tasks/restore', ['id' => $id->content()])->assertStatus(200);

        $tasks = $this->get('ajax/tasks/get')->assertSee($task['Title']);
    }



}
