<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'TaskController@Index')->middleware('auth')->name('Tasks');

Route::prefix('ajax/tasks')->middleware('auth')->group(function() {
    Route::get('get', 'TaskController@Get')->name('GetTasks');
    Route::post('create', 'TaskController@Create')->name('CreateTask');
    Route::post('edit', 'TaskController@Edit')->name('EditTask');
    Route::post('delete', 'TaskController@Delete')->name('DeleteTask');

    Route::get('getdeleted', 'TaskController@GetDeleted')->name('GetDeletedTasks');
    Route::post('restore', 'TaskController@Restore')->name('RestoreTask');
});


Route::prefix('ajax/category')->middleware('auth')->group(function() {
    Route::get('/get', 'CategoryController@Get')->name('GetCategories');
    Route::post('create', 'CategoryController@Create')->name('CreateCategory');
    Route::post('edit', 'CategoryController@Edit')->name('EditCategory');
    Route::post('delete', 'CategoryController@Delete')->name('DeleteCategory');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->middleware('auth')->name('home');
