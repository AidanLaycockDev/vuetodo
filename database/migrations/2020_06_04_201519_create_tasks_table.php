<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('Title');
            $table->string('Description')->nullable();
            $table->dateTime('Due_date')->nullable();
            $table->boolean('Complete')->default(false);
            $table->unsignedBigInteger('Category_id');
            $table->unsignedBigInteger('User_id');

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('Category_id')->references('id')->on('Categories');
            $table->foreign('User_id')->references('id')->on('Users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
